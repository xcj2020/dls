/*****************************************************************************
 *
 * Copyright (C) 2021  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the data logging service (DLS).
 *
 * The DLS is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * The DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QMainWindow>

#include "DlsWidgets/Model.h"
#include "DlsWidgets/Graph.h"

/****************************************************************************/

class MainWindow:
    public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget * = 0);
        ~MainWindow();

 private:
        QtDls::Model dlsModel;
	DLS::Graph dlsGraph;

};

/****************************************************************************/
