/*****************************************************************************
 *
 * Copyright (C) 2021  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the data logging service (DLS).
 *
 * The DLS is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * The DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MainWindow.h"

#include "DlsWidgets/Translator.h"

#include <QApplication>
#include <QDebug>

/****************************************************************************/

int main(int argc, char *argv[])
{

    QCoreApplication::setOrganizationName("EtherLab");
    QCoreApplication::setOrganizationDomain("etherlab.org");
    QCoreApplication::setApplicationName("dlsMini");

    QApplication app(argc, argv);

    // retrieve system locale
    QLocale::setDefault(QLocale(QLocale::system().name()));

    MainWindow mainWin(0);
    mainWin.show();

    try {
        return app.exec();
    }
    catch(QtDls::Model::Exception &e) {
        qCritical() << "Model exception: " << e.msg;
        return 1;
    }
}


/****************************************************************************/
