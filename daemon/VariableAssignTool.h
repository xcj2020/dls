/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef VARIABLE_ASSIGN_TOOL_H
#define VARIABLE_ASSIGN_TOOL_H

#include <pdcom5/Variable.h>
#include <string>

class FindProxy;

class VariableAssignTool
{
  public:
    enum class SpinResult {
        CallAgain, /**< call spin() again */
        DataNeeded, /**< call Process::asyncData() */
        Finished, /**< Task completed. */
        Error, /**< Error occured */
    };


    virtual SpinResult spin(FindProxy const& find_proxy) = 0;
    virtual void setNextVariable(PdCom::Variable var) = 0;
    virtual ~VariableAssignTool() = default;
};


#endif  // VARIABLE_ASSIGN_TOOL_H
