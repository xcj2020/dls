#!/bin/bash

# Compilation of all requisites for dlsgui and of dlsgui itself
#
# This is meant to be called from inside the Win64 build container.
# ./bootstrap has to be called before from outside the build container.

set -e

MINGW_ROOT=/usr/x86_64-w64-mingw32/sys-root/mingw
BUILD_ROOT=$(pwd -P)/build-root

REV=$(git -C . describe || echo "");

mkdir -p $BUILD_ROOT

mingw64-cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_DAEMON=0 \
    -DBUILD_DLS_TOOL=0 \
    -DBUILD_FLTK_GUIS=0 \
    -DDLS_DESIGNER=0 \
    -DDLS_HDF5_EXPORT=0 \
    -DCMAKE_INSTALL_PREFIX=$BUILD_ROOT \
    -DCMAKE_INSTALL_LIBDIR=$BUILD_ROOT/lib

make -j$(nproc) install

TARGET=dlsgui-win64-$REV
rm -rf $TARGET
mkdir $TARGET
gui/copy-dlls.sh $TARGET
cp $BUILD_ROOT/bin/*.dll $TARGET
cp $BUILD_ROOT/bin/dlsgui.exe $TARGET

zip -r $TARGET.zip $TARGET
rm -r $TARGET $BUILD_ROOT
