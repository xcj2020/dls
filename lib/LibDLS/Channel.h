/*****************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef LibDLSChannelH
#define LibDLSChannelH

/****************************************************************************/


#include "Exception.h"
#include "Time.h"

#include "Chunk.h"
#include "globals.h"

#include <string>
#include <map>
#include <utility> // pair
#include <set>
#include <memory>

/****************************************************************************/

namespace DlsProto {
    class ChannelInfo;
}

namespace LibDLS {

/****************************************************************************/

class Job;

/****************************************************************************/

/**
   Channel exception.
*/

class DLS_PUBLIC ChannelException:
    public Exception
{
    public:
        ChannelException(const std::string &pmsg):
            Exception(pmsg) {};
};

/****************************************************************************/


/**
   Darstellung eines Kanals in der Anzeige
*/

class DLS_PUBLIC Channel
{
public:
    Channel(Job *);
    Channel(Job *, const DlsProto::ChannelInfo &);
    Channel(const Channel &);
    ~Channel();

    Job *getJob() const;

    void import(const std::string &, unsigned int);
    std::pair<std::set<Chunk *>, std::set<int64_t> > fetch_chunks();
    void fetch_data(Time, Time, unsigned int,
                    DataCallback, void *, unsigned int = 1);

    std::string path() const;
    unsigned int dir_index() const;

    const std::string &name() const;
    const std::string &unit() const;
    std::string alias() const;
    double scale() const;
    double offset() const;
    std::string color() const;
    ChannelType type() const;

    typedef std::map<int64_t, Chunk> ChunkMap;
    const ChunkMap &chunks() const;
    bool has_same_chunks_as(const Channel &) const;

    Time start() const;
    Time end() const;

    bool operator<(const Channel &other) const;

    void set_channel_info(DlsProto::ChannelInfo *) const;
    void set_chunk_info(DlsProto::ChannelInfo *) const;

    void update_index();

private:
    struct Impl;
    std::unique_ptr<Impl> impl;

    Channel();
};

} // namespace

/****************************************************************************/

#endif
