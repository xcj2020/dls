/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <iostream>
#include <sstream>
#include <limits>
#include <iomanip>
#include <algorithm>

#include "LibDLS/Export.h"
#include "LibDLS/Job.h"
#include "LibDLS/JobPreset.h"
#include "LibDLS/Time.h"

#include "Export_p.h"
#include "File.h"

using std::endl;

using namespace LibDLS;

/*****************************************************************************/

Export::Impl::Impl():
    trim(false)
{
}

/******************************************************************************
 * Export
 *****************************************************************************/

Export::Export():
    _impl(new Impl())
{
}

/*****************************************************************************/

Export::Export(Impl * impl) : _impl(impl) {}

/*****************************************************************************/

Export::~Export()
{
}

/*****************************************************************************/

void Export::setReferenceTime(const Time &ref)
{
    _impl->referenceTime = ref;
}

/*****************************************************************************/

void Export::setTrim(const Time &start, const Time &end)
{
    _impl->trim = true;
    _impl->trimStart = start;
    _impl->trimEnd = end;
}

/*****************************************************************************/

void Export::open(
		  const std::string &path,
		  const std::string &filename,
		  const Time &start,
		  const Time &end
		  )
{
    (void) path;
    (void) filename;
    (void) start;
    (void) end;
}

/*****************************************************************************/

void Export::close()
{
}

/*****************************************************************************/

void Export::addMessage(const LibDLS::Job::Message &m)
{
    (void) m;
}

/******************************************************************************
 * ExportAscii
 *****************************************************************************/

ExportAscii::ExportAscii()
{
}

/*****************************************************************************/

ExportAscii::~ExportAscii()
{
}


/*****************************************************************************/

void ExportAscii::begin(
        const Directory &dls_dir,
        const Channel &channel,
        const std::string &path,
        const std::string &filename
        )
{
    (void) dls_dir;
    std::stringstream filepath;

    filepath << path << "/";

    if (filename.empty()) {
        filepath << "channel" << channel.dir_index();
    }
    else {
        filepath << filename;
    }

    filepath << ".dat";
    _file.open(filepath.str().c_str(), std::ios::trunc);

    if (!_file.is_open()) {
        std::stringstream err;
        err << "Failed to open file \"" << filepath.str() << "\"!";
        throw ExportException(err.str());
    }

    _file << "% --- DLS exported data ---" << endl;
    _file << "%" << endl;
    _file << "% Channel: " << channel.name() << endl;
    _file << "%    Unit: " << channel.unit() << endl;
    _file << "%" << endl;

    _file << std::setprecision(std::numeric_limits<long double>::digits10);
}

/*****************************************************************************/

void ExportAscii::data(const Data *data)
{
    using std::fixed;
    unsigned int i;

    for (i = 0; i < data->size(); i++) {
        Time time(data->time(i));
        if (!_impl->trim ||
                (time >= _impl->trimStart && time <= _impl->trimEnd)) {
            _file << fixed << time - _impl->referenceTime << "\t"
                << fixed << data->value(i) << endl;
        }
    }
}

/*****************************************************************************/

void ExportAscii::end()
{
    _file.close();
}

/******************************************************************************
 * ExportMat4
 *****************************************************************************/

ExportMat4::ExportMat4()
{
    _file = new File();
}

/*****************************************************************************/

ExportMat4::~ExportMat4()
{
    delete _file;
}

/*****************************************************************************/

void ExportMat4::begin(
        const Directory &dls_dir,
        const Channel &channel,
        const std::string &path,
        const std::string &filename
        )
{
    (void) dls_dir;
    std::stringstream name;

    if (filename.empty()) {
        name << "channel" << channel.dir_index();
    }
    else {
        name << filename;
    }

    _header.type = 0000; // Little-Endian, double, numeric (full) matrix
    _header.mrows = 2;
    _header.ncols = 0; // set later
    _header.imagf = 0; // only real data, no imaginary part
    _header.namelen = name.str().size() + 1;

    std::stringstream filepath;
    filepath << path << "/" << name.str() << ".mat";
    _file->open_read_write(filepath.str().c_str());

    _file->write((const char *) &_header, sizeof(Mat4Header));
    _file->write(name.str().c_str(), name.str().size() + 1);
}

/*****************************************************************************/

void ExportMat4::data(const Data *data)
{
    unsigned int i;
    double val;

    for (i = 0; i < data->size(); i++) {
        Time time(data->time(i));
        if (!_impl->trim ||
	    (time >= _impl->trimStart && time <= _impl->trimEnd)) {
            val = (data->time(i) - _impl->referenceTime).to_dbl();
            _file->write((const char *) &val, sizeof(double));
            val = data->value(i);
            _file->write((const char *) &val, sizeof(double));
            _header.ncols++;
        }
    }
}

/*****************************************************************************/

void ExportMat4::end()
{
    _file->seek(0); // write header again, this time with number of columns
    _file->write((const char *) &_header, sizeof(Mat4Header));
    _file->close();
}

/*****************************************************************************/
