#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

protobuf_generate_cpp(PROTO_SRC PROTO_HDR ../proto/dls.proto)

add_library(dls
    Base64.cpp
    BaseMessage.cpp
    BaseMessageList.cpp
    Channel.cpp
    ChannelPreset.cpp
    Chunk.cpp
    Data.cpp
    Dir.cpp
    Export.cpp
    File.cpp
    globals.cpp
    Job.cpp
    JobPreset.cpp
    mdct.cpp
    Time.cpp
    XmlParser.cpp
    XmlTag.cpp
    ZLib.cpp
    ${PROTO_SRC}
    ${PROTO_HDR}
)

if (DLS_HDF5_EXPORT)
    target_sources(dls PRIVATE HDF5Export.cpp)
    target_link_libraries(dls PRIVATE
        ${HDF5_LIBRARIES}
    )

    target_include_directories(dls PRIVATE
        ${HDF5_INCLUDE_DIRS}
    )

    target_compile_definitions(dls PRIVATE
        ${HDF5_DEFINITIONS}
    )
    target_compile_definitions(dls PUBLIC
        DLS_HDF5_EXPORT
    )
endif()

target_compile_options(dls PRIVATE -Wall)

set(DLS_PUBLIC_HEADERS
    LibDLS/Channel.h
    LibDLS/ChannelPreset.h
    LibDLS/Chunk.h
    LibDLS/Data.h
    LibDLS/Dir.h
    LibDLS/Exception.h
    LibDLS/Export.h
    LibDLS/globals.h
    LibDLS/Job.h
    LibDLS/JobPreset.h
    LibDLS/Time.h
)

target_include_directories(dls PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/..>"
    "$<INSTALL_INTERFACE:include>"
)

target_link_libraries(dls PRIVATE
    ${FFTW3_LIBRARIES}
    ${LIBXML2_LIBRARIES}
    ${PCRE_LIBRARIES}
    ${PROTOBUF_LIBRARIES}
    Threads::Threads
    ${ZLIB_LIBRARIES}
    uriparser::uriparser
    ${EXTRA_PROTOBUF_LIBS}
)

target_include_directories(dls PRIVATE
    ${FFTW3_INCLUDE_DIRS}
    ${LIBXML2_INCLUDE_DIR}
    ${PCRE_INCLUDE_DIRS}
    ${PROTOBUF_INCLUDE_DIRS}
    ${ZLIB_INCLUDE_DIRS}
)

if (NOT WIN32)
    target_link_libraries(dls PRIVATE -lm)
else()
    target_link_libraries(dls PRIVATE ws2_32)
endif()

set_target_properties(dls PROPERTIES
    PUBLIC_HEADER "${DLS_PUBLIC_HEADERS}"
    SOVERSION 7
    VERSION 7.0.0
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED 1
)

## install target and headers

install(TARGETS dls
    EXPORT dlsTargets
    ARCHIVE DESTINATION       "${CMAKE_INSTALL_LIBDIR}"
    LIBRARY DESTINATION       "${CMAKE_INSTALL_LIBDIR}"
    RUNTIME DESTINATION       "${CMAKE_INSTALL_BINDIR}"
    PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/LibDLS"
)

set(ConfigPackageLocation "${CMAKE_INSTALL_LIBDIR}/cmake/dls")
configure_package_config_file(Config.cmake.in
        "${CMAKE_CURRENT_BINARY_DIR}/dls-config.cmake"
    INSTALL_DESTINATION ${ConfigPackageLocation}
)
write_basic_package_version_file(
        "${CMAKE_CURRENT_BINARY_DIR}/dls-configVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

install(EXPORT dlsTargets
    NAMESPACE EtherLab::
    FILE dlsTargets.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/dls
)
install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/dls-config.cmake
        ${CMAKE_CURRENT_BINARY_DIR}/dls-configVersion.cmake
    DESTINATION ${ConfigPackageLocation}
)
export(EXPORT dlsTargets
    FILE "${CMAKE_CURRENT_BINARY_DIR}/dlsTargets.cmake"
    NAMESPACE EtherLab::
)
