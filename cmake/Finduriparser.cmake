#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

find_package(uriparser CONFIG QUIET)
if(NOT uriparser_FOUND)
    find_package(PkgConfig)
    pkg_check_modules(URIPARSER REQUIRED liburiparser)
    if (NOT URIPARSER_LINK_LIBRARIES)
        find_library(URIPARSER_LINK_LIBRARIES "${URIPARSER_LIBRARIES}" PATHS "${URIPARSER_LIBRARY_DIRS}")
    endif()

    find_package_handle_standard_args(uriparser DEFAULT_MSG URIPARSER_LINK_LIBRARIES)

    if (uriparser_FOUND)
        add_library(uriparser::uriparser UNKNOWN IMPORTED)
        set_target_properties(uriparser::uriparser PROPERTIES
            IMPORTED_LOCATION "${URIPARSER_LINK_LIBRARIES}"
            INTERFACE_INCLUDE_DIRECTORIES "${URIPARSER_INCLUDE_DIRS}"
        )
    endif()

endif()
