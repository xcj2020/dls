#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.6)

project(dls VERSION 1.5.4)

include(CMakeDependentOption)
include(CMakePackageConfigHelpers)
include(GNUInstallDirs)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)
option(BUILD_DAEMON "Build DLS Daemon" ON)
option(DLS_SERVER "Build DLS protocol server" ON)
option(DLS_PROTO_DEBUG "Debug protocol" OFF)
option(BUILD_FLTK_GUIS "Build FLTK GUIs" ON)
option(BUILD_DLS_TOOL "Build commandline tool" ON)
option(BUILD_WIDGETS "Build DLS Widgets" ON)
option(DLS_HDF5_EXPORT "Enable HDF5 Export" ON)
cmake_dependent_option(DLS_DESIGNER "Build DLS Widgets as Qt Designer Plugin" ON "BUILD_WIDGETS" OFF)
cmake_dependent_option(DLS_GUI "Build DLS GUI" ON "BUILD_WIDGETS" OFF)

# Place for dls config file, depends on linux distribution
if (NOT DLS_ENVIRONMENT_FILE_LOCATION)
    set(DLS_ENVIRONMENT_FILE_LOCATION "${CMAKE_INSTALL_FULL_SYSCONFDIR}/sysconfig")
endif()

find_package(LibXml2 REQUIRED)
find_package(PkgConfig)
pkg_check_modules(FFTW3 REQUIRED fftw3)
pkg_check_modules(PCRE REQUIRED libpcre)
find_package(Protobuf REQUIRED MODULE)
if (Protobuf_VERSION VERSION_GREATER 4.22 OR Protobuf_VERSION VERSION_EQUAL 4.22)
    # workaround: FindProtobuf.cmake does not mention public dependencies
    # and the *config.cmake files of Protobuf are not shipped on SuSE.
    find_package(absl REQUIRED CONFIG)
    set(EXTRA_PROTOBUF_LIBS
        absl::absl_check
        absl::absl_log
        absl::algorithm
        absl::base
        absl::bind_front
        absl::bits
        absl::btree
        absl::cleanup
        absl::cord
        absl::core_headers
        absl::debugging
        absl::die_if_null
        absl::dynamic_annotations
        absl::flags
        absl::flat_hash_map
        absl::flat_hash_set
        absl::function_ref
        absl::hash
        absl::layout
        absl::log_initialize
        absl::log_severity
        absl::memory
        absl::node_hash_map
        absl::node_hash_set
        absl::optional
        absl::span
        absl::status
        absl::statusor
        absl::strings
        absl::synchronization
        absl::time
        absl::type_traits
        absl::utility
        absl::variant
    )
else()
    set(EXTRA_PROTOBUF_LIBS "")
endif()

find_package(uriparser REQUIRED)
find_package(Threads REQUIRED)
find_package(ZLIB REQUIRED)
if (DLS_HDF5_EXPORT)
    find_package(HDF5 REQUIRED COMPONENTS C CXX)
endif ()

if (BUILD_WIDGETS)
    set(REQUIRED_QT_COMPONENTS Core Gui LinguistTools PrintSupport Svg Widgets Xml)
    if (DLS_DESIGNER)
        list(APPEND REQUIRED_QT_COMPONENTS Designer)
    endif()

    find_package(Qt5 REQUIRED COMPONENTS ${REQUIRED_QT_COMPONENTS})
endif()

if (NOT REVISION)
    execute_process(COMMAND sh "${CMAKE_CURRENT_SOURCE_DIR}/revision.sh" "${CMAKE_CURRENT_SOURCE_DIR}"
        OUTPUT_VARIABLE REVISION
        ERROR_VARIABLE REVISION_RESULT
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (NOT ${REVISION_RESULT} EQUAL 0)
        message(FATAL_ERROR "Could not get revsision, is git installed?")
    endif()
endif()

configure_file(config_cmake.h.in "${CMAKE_CURRENT_BINARY_DIR}/config.h" @ONLY)

add_subdirectory(lib)
add_subdirectory(script)

if (BUILD_DAEMON)
    find_package(pdcom5 REQUIRED)
    add_subdirectory(daemon)
endif()
if (BUILD_FLTK_GUIS)
    find_package(FLTK REQUIRED)
    add_subdirectory(fltkguis)
endif()
if (BUILD_DLS_TOOL)
    add_subdirectory(tool)
endif()

if (BUILD_WIDGETS)
    add_subdirectory(widgets)
    if (DLS_GUI)
        add_subdirectory(gui)
    endif()
endif()
